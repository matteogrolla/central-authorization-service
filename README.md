### Installazione dipendenze
pipenv install

### Avviare esempio
cd python-webservice-template
pipenv shell
python app.py 

la swagger ui è disponibile all'url  
http://127.0.0.1:9000/ui/#/

### Generazione del model ###

i files presenti direttamente sotto la cartelle swagger_server non vanno 
sovrascritti, in quanto sono stati modificati a partire da quelli generati
automaticamente.
Il contenuto della cartella swagger_server/models va sovrascritto con il codice
 generato a partire dalle specifiche open api.
Si può usare indifferentemente
- swagger codegen
- l'interfaccia web https://app.swaggerhub.com/
il formato di output deve essere python/flask

### Import del progetto in intellij ###

Questi sono i passi che ho seguito io (potrebbe esserci un percorso migliore)
all'interno di un progetto idea
da project structure

    import module from existing sources
    intellij lo riconosce come modulo python

    in platform settings - SDK
    python SDK
      pipenv environment
        in associated module
        selezionare python-webservice-template  (può essere necessario riaprire intellij)
        
    in modules - python-webservice-template
       nel tab SDK
       selezionare pipenv(python-webservice-template)
 
  
