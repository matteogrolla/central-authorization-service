import unittest

from services.jwt_service import JwtService


class JwtServiceTests(unittest.TestCase):

    def setUp(self) -> None:
        self.maxDiff = None
        self.jwt_service = JwtService()
        self.cerved_jwt_token_svil = 'eyJhbGciOiJSUzUxMiJ9.ZXlKaGRXUWlPaUpCVkNJc0luTjFZaUk2SWtOSFJUQXhOemd3SWl3aVlYVjBhRzl5YVhSNUlqb2lRVkJKUTBWU1ZrVkVJaXdpYVhOeklqb2lhSFIwY0hNNlhDOWNMMk5zZFhOMFpYSjNaV0l1YkdsdVkyVXVhWFJjTDJOaGN5SXNJbVY0Y0NJNk1UVTBPVGt5TlRNME1pd2lhV0YwSWpveE5UUTVPRGsyTlRReUxDSnFkR2tpT2lKVFZDMHhPUzFoVUROWk5XeGhURU0wY2pWdmFIazNZbWxwWVVKc2VVZzNURmt0TWpRNVl6UTRORFl3TldVeUluMD0.WcqWerv50qs0aCHq8YacRvKtQdMh4CzTo-K_N-3REh7uXCXeBaA4hG5Fg0Jns3BzTWA3-ZTIL7cLECpQLPAdkezWyr01NaopQG2w-d0D6RLFE3OqCY8sPuyO_8LlTd2S99z-gRiCgv23Gyrs9X9Fk3LQOB-3-LOV9c2gqK_ciQHGMKaAVT7YievjjrXlVcWVkjteL9K3B-YmVvroQROJPLwBNBtweFEh48UJRp5KaR2bqs-1vvyIpcSScFcpHOnJUDuoUwN3XfwFhmVdGZqUPGvYFMOenXrVziQS9NPNjKn2ZC4P8p7wFxzQbTlvqrtCBHzOBjLXEIN1QOsbFWQeNQ'

    def test_validate_cerved_jwt_token(self):
        is_valid = self.jwt_service.validate_cerved_jwt_token(self.cerved_jwt_token_svil)
        self.assertFalse(is_valid, "should be invalid")

    def test_decode_cerved_jwt_token(self):
        decoded = self.jwt_service.decode_cerved_jwt_token(self.cerved_jwt_token_svil)
        expected = {
            'aud': 'AT',
            'sub': 'CGE01780',
            'authority': 'APICERVED',
            'iss': 'https://clusterweb.lince.it/cas',
            'exp': 1549925342,
            'iat': 1549896542,
            'jti': 'ST-19-aP3Y5laLC4r5ohy7biiaBlyH7LY-249c484605e2'
        }
        self.assertEqual(
            expected,
            decoded
        )

    def test_build_news_nwt_token(self):
        news_jwt_token = self.jwt_service.build_news_jwt_token(self.cerved_jwt_token_svil)
        expected = b'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJDR0UwMTc4MCIsImF1dGhvcml0eSI6IkFQSUNFUlZFRCIsImlzcyI6Imh0dHBzOi8vY2x1c3RlcndlYi5saW5jZS5pdC9jYXMiLCJleHAiOjE1NTU5MjUzNDIsImlhdCI6MTU0OTg5NjU0MiwianRpIjoiU1QtMTktYVAzWTVsYUxDNHI1b2h5N2JpaWFCbHlIN0xZLTI0OWM0ODQ2MDVlMiIsInNlcnZpY2VzIjp7fX0.PfO9m8E1OD2gjOB9y6nzgV_YbT2gRLTJA-FYN7T8nCgMKDLFWTqR1akHDDIQiFgRj2XoGEul-38rffqADo6LsNN4rK3kucn6D1PdSUe2ftiZ_hJy0avPkbzt8swz5frPfLB-nLTaxbBtMalacNaxwIBwoPLSRfFghM--DQSyxogVgHTti-tcAMqb6hd7L-vNoGbF_s-qnoaqu2CtpVjAqLhLf6iNS1ZRDveSf8Lil38iM4idt9VHidghNU3yWwXdMQWBptQyRW5lixyad5yy-ViE2LFXARWTzdGlLYpHGobsQXR62HBjD9GlVB5Z_dzAIX0A5hvxjYg-Rxx8zwGR7A'
        self.assertEqual(
            expected,
            news_jwt_token
        )