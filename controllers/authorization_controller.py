import datetime

from services.jwt_service import JwtService
from swagger_server.models import InventoryItem

jwt_service = JwtService()

def get_cervednews_token(cerved_token):
    cervednews_token = jwt_service.build_news_jwt_token(cerved_token)
    return cervednews_token


