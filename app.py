#!/usr/bin/env python3
import json
import logging

import flask
import sys

import connexion
from connexion.apps.flask_app import FlaskJSONEncoder
from flask import render_template
from flask_cors import CORS
# from pymongo import MongoClient

# import handlers.app_handlers
# import handlers.app_customers
# from LuceneQueryValidator import LuceneQueryValidator
# from app_customizations import CustomJSONEncoder, CustomJSONDecoder
# from handlers.app_handlers import *
# from handlers.app_customers import *
# from handlers.app_authentication import *

# import controllers
from controllers.authorization_controller import *


def init_components(env_id):
    global logger

    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    logging.info("started")

    # with open(f'env_{env_id}.json', 'r') as f:
    #     config = json.load(f)

    # logger.info(f"configuration: {config}")
    # client = MongoClient(config["MongoDB"]["host"], config["MongoDB"]["port"])
    # customers_db = client[config["MongoDB"]["customers"]["db"]]
    # customers_collection = customers_db[config["MongoDB"]["customers"]["collection"]]
    # languages_db = client[config["MongoDB"]["languages"]["db"]]
    # languages_collection = languages_db[config["MongoDB"]["languages"]["collection"]]
    #
    # luceneQueryValidator = LuceneQueryValidator(
    #     host=config["Elasticsearch"]["host"],
    #     port=config["Elasticsearch"]["port"],
    #     query_validation_db=config["Elasticsearch"]["query_validation_db"])


def wire_components():
    # handlers.app_handlers.logger = logger
    # handlers.app_handlers.languages_collection = languages_collection
    # handlers.app_customers.logger = logger
    # handlers.app_customers.customers_collection = customers_collection
    pass


class ReactFlaskApp(connexion.App):

    def create_app(self):
        app = flask.Flask(self.import_name, static_url_path='/assets', static_folder='assets')
        app.json_encoder = FlaskJSONEncoder
        return app

# TODO: collection indexes must be created by the application
# ex: customer.id is a unique index
if __name__ == '__main__':

    env_id = sys.argv[1] if len(sys.argv) >= 2 else 'local'

    init_components(env_id)
    wire_components()

    connexion_app = ReactFlaskApp(__name__)
    # set the WSGI application callable to allow using uWSGI:
    # uwsgi --http :8080 -w app
    application = connexion_app.app
    # application.json_encoder = CustomJSONEncoder
    # application.json_decoder = CustomJSONDecoder

    @connexion_app.route('/index2.html')
    def index():
        return render_template('/index.html')

    connexion_app.add_api('swagger.yaml')

    # add CORS support
    CORS(application)
    # run our standalone gevent server
    connexion_app.run(port=9001, server='gevent')
