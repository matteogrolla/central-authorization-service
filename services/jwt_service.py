import json

import jwt
from jwt.utils import base64url_decode

# JWT tokens are URL-safe when it comes to their syntax. Here is a quote from the RFC 7519:
# A JWT is represented as a sequence of URL-safe parts separated by period (.) characters.
# Each part contains a base64url-encoded value. [...]

# However, when using JWT as bearer tokens, it's advisable to avoid sending them in the URL.
# See the following quote from the RFC 6750:
# Don't pass bearer tokens in page URLs: Bearer tokens SHOULD NOT be passed in page URLs (for example, as query string parameters).
# Instead, bearer tokens SHOULD be passed in HTTP message headers or message bodies for which confidentiality measures are taken.
# Browsers, web servers, and other software may not adequately secure URLs in the browser history, web server logs,
# and other data structures. If bearer tokens are passed in page URLs, attackers might be able to steal them from the history data,
# logs, or other unsecured locations.

class JwtService():

    def __init__(self):
        self.pk_cerved_sv = b'-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApD4AJHTMVnJGPTi6Juenu/leEuuO3lMcawc/PAfpc8ipgdE19sAoOpJE8k28sdRuvbJGj4QombBNH3RqSnDtIiO51H9CvIOsSH8Xy4kUxB+APamE3VgJlh3zTlXXYL4IVpXr6KAmBBctXnOtxcj/n8mc8i4eT3ayKabW6iXVXORTICzoXgw+lLrwTBjA+AE4kxzTvoi3ion1sG/m/cWDBPfafksdJTZRLwB+kg50Dhy1XGg8ZZdrpisg05e6B5JVnHcJVArlJqyzYGQ2g/2CF2uEKH5whtV0g1DqwcZxij0hvfA0xSNzoaK8oX0ZZJe1Qq0QnnAFQqIUZPI+2GA4jQIDAQAB\n-----END PUBLIC KEY-----'
        self.private_key = b"""-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEA05IDL+Y6VaJvUWmI4vOHG0mL3h8TqfQ/icg6PBiA01MPj/dH
zM8mTbxsRlxEbtIHb82mOJeWavd+TmiLSPNXpbcNu4ZoY+LCmpxf3C2Uk3kbL7AP
IOEw56QTDCH9znscRC4r75uXEfv38FCXySU+uWmILAXXqEHHiFW2q4ieR6mHvR7q
Z4gg3uJARsCMGkHMofOTtkVwjbh56lQboMWY8vV0ap6fg7OuRjWt4RF5fd4kU3mW
YLlJPnMqcjPifiCLzlqF4EP0lfcLRwHjMuD/oFQers8auQMYKouhgqNuClBI4JZL
znK9qULr5fuGjvJI5fS7UIY1yyvwx6NSlmSMnQIDAQABAoIBAHYZcGwBgRiiRq9U
UNQXCpkB0fCYulpJfFpGCbPYHblZCTvc1hiOnZwRDQOtp4mQ9hPD4QCAJbfqFXhh
RTbgl1FqwGCoY4c2Ylj550y5qNE1biaNibRZ3/m+Dd/c7ZegAyRP/biPyqGAXGOq
KUBf6QaI0Tl5vWsv5NjGt+RCq8/l3xh5Omsxm20pW1av8Bu8tTfQFV7uAc/E6vAQ
oG8BJTXIp1dE6TBJgH89ziZ9OOyDa/7wb8vNVzTF5DFskEJXzmjx375HvCsOs2wE
81RNF4c9bayYSoT9HH0UYeXBTaFZOYsyOnhYXjDquYZVGg8nye1f0VtnUJ4bRD1W
lk3RXwUCgYEA9tnZxfmw0LeUWIf2o3yWeDNNkZ8nThvH3ah0FoSs9fJCFtdB2i3T
2M1NwPcRIKEPPSoPR/akSeBUdw9GvNfZTUCGIoNjWqsrRwsSFRMFq0m3Bwz9oKWf
BtJOnoTg+L7KhUVVqiP9iE5Z0gA+AzH9xqHU+jGBCNIdaLwHzjb27ccCgYEA22lq
Vhbi6we3w6Z/WULzhO/uMi+dQ+AEVtUY+LD2jIweVTMnskk9D1TvatZQYL5vdq8Q
HRs3TPVT0GzPFjhV66HK1p2Ba9TJ8ZpMnUczzbvNSy15K8aLNcROWGmJwJ8iEUA9
qOeQSwhCon5p4T5coqTLEa2aNuD4oD1My2P1QnsCgYBIU3gtO4WFAeTjM6MeL6je
uxllK16W0ijzVyH8Xw9WWI7oplVTGc5SXYUEnt9T0hzm7OXhTfDA/IuC/uTY+91d
dyRWBG0d6b0aabH4nsQwdb68wB+n7B72HrSOWZyZBT3NYo1A9p6YS2yhzRReV01l
35DJZavK69VWFJGs6AndjQKBgQCGm/rIDEx1IwwDtY8lpSSfp098bKfQL4yZ9SzX
zkHEYmZtoO+2uqYCVPp5kjhH0i2yI58ALGVWCqC8GzVFjZRw9pbp6kGvdSHb3u0o
BLnse05s094mFC3t85yZM0525WpSKRZrHH3y+oNfC61LqiXNBssq8lXNW2JNdK47
jfy3JwKBgEUTx4aoqeKtN/IWDknnk3dEQ+Ev6RCfpIi5BluiJh0tN3jPCOKEvX3C
gwx/wc1sETsjrIK0xM8WfV6At6l5bw4GTf6YFOyhm1zKqcV8kTf9Nnjtpo9j3nzi
SsoU8fNiUjAukt/NxhH6g9C4nwhjq1HYbuN9EtEwVPMlatztvbKS
-----END RSA PRIVATE KEY-----"""

    def validate_cerved_jwt_token(self, token: str):
        # validazione e decodifica del token cerved
        # a causa del doppio encoding del payload la libreria lancia un DecodeError se il token è valido, altrimenti un'altra eccezione
        try:
            jwt.decode(token, self.pk_cerved_sv, algorithms='RS512', verify=True)
        except jwt.exceptions.DecodeError:
            return False
        except Exception:
            return True

    def decode_cerved_jwt_token(self, token: str):
        parts = token.split('.')
        binary_payload = base64url_decode(base64url_decode(parts[1]))
        payload = json.loads(binary_payload)
        return payload

    def build_news_jwt_token(self, cerved_jwt_token: str) -> bytes:
        # TODO: if is valid
        payload = self.decode_cerved_jwt_token(cerved_jwt_token)

        # advance exp date
        # TODO: quanto devono rimanere validi i token?
        # forse è una buona idea falo durare 10 12 ore almeno finchè non avremo un modo per fare il refresh del token
        # in modo trasparente all'utente
        payload['exp'] = 1555925342
        # remove aud to avoid it's check
        del payload['aud']

        payload["services"] = {}
        # TODO: richiedere ad ogni servizio le UserAuthorizations da inserire in payload["services"]

        cervednews_token_bytes = jwt.encode(payload, self.private_key, algorithm='RS256')
        cervednews_token = cervednews_token_bytes.decode('UTF-8')

        return cervednews_token
